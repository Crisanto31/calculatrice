# Configuration des outils


## 1. Python

Il est recommandé d'utiliser un [environnement virtuel](./installation_et_utilisation_du_code.md)

Pour ajouter des packages Python, utiliser la commande `pip install` en mentionnant l'adresse et port du proxy:

`> pip install --proxy=https://<compte>:<secret>@proxy-surf.loc.cnes.fr:8050  <package>`

Il est aussi recommandé d'utiliser la dernière version de pip:

`> python -m pip install --proxy=https://<compte>:<secret>@proxy-surf.loc.cnes.fr:8050 --upgrade pip`

où `<compte>` et `<secret>` sont les informations personnelles d'accès au SI du CNES (ceux du PC bureautique CNES).

## 2. Gitlab

### 2.1 Pouvoir utiliser un client Git dans son PC local
suivre la procédure dans 
https://confluence.cnes.fr/pages/viewpage.action?pageId=26166114

<img src="gitlab_acces_interne.png" alt="gitlab_acces_interne" width="200"/>


A la fin de cette procedure:

1. Ces fichiers ont été créées et configurés dans le PC local (avec git-bash):
<img src="gitlab_acces_interne_ssh.png" alt="gitlab_acces_interne_ssh" width="200"/>

2. la clé publique est enregistrée dans gitlab (compte personnel): 
https://gitlab.cnes.fr/-/profile/keys
<img src="gitlab_cle_ssh.png" alt="gitlab_cle_ssh.png" width="200"/>

3. tortoiseGit est fonctionnel avec la clef Putty ingérée:

<img src="gitlab_acces_interne_tortoisegit.png" alt="gitlab_acces_interne_tortoisegit.png" width="200"/>

### 2.2 Permettre à Jenkins l'accès à Gitlab

Pour que Jenkins puisse compiler votre projet, celui-ci doit avoir un accès à votre dépôt pour faire un checkout des sources. Cet accès doit être sécurisé mais ne doit pas requérir la saisie d'un login/mot-de-passe. La solution réside soit dans l'utilisation d'une paire de clefs SSH appelée clef de déploiement (Deploy Key) dans le langage de GitLab soit d'utiliser un Deploy Token. 

**Attention**: Il ne s'agit cependant pas des mêmes clés que pour l'accès interne au CNES (section ci-dessus).

Création de clés avec Git-Bash et enregistrement dans Gitlab et Jenkins:

1. suivre la procédure dans https://confluence.cnes.fr/pages/viewpage.action?pageId=24092531
<img src="gitlab_acces_jenkins.png" alt="gitlab_acces_jenkins" width="200"/>

2. La procédure de création de clé SSH doit avoir été activée dans Gitlab (cf lien ci-dessus)
Cette clef peut s'appeler *gitlab-jenkins-acces-key* et sera utilisée/mentionnée dans l'interface Jenkins (ci-dessous)
<img src="calculatrice-bagotd-access-key.png" alt="calculatrice-bagotd-access-key" width="200"/>

3. Enregistrer la partie privée de la paire de clefs SSH dans Jenkins pour que celui-ci l'utilise pour accéder à votre dépôt. cf cette page [Jenkins - Gestion des identifiants et des secrets](https://confluence.cnes.fr/display/USINELOG/Jenkins+-+Gestion+des+identifiants+et+des+secrets#Jenkins-Gestiondesidentifiantsetdessecrets-Cr%C3%A9ationd%27unsecret%22SSHUsernamewithprivatekey%22)

# 3. Jenkins


## 3.1 Initialiser un projet Jenkins

1. Aller dans le groupe SANDBOX_Qualite: https://jenkins-ci.cnes.fr/job/SANDBOX_Qualite/

2. Créer un dossier Jenkins dédié, par exemple crééer le répertoire <compte_SandBox>
Jenkins -> New Element -> Dossier

3. Crééer un projet Pipeline Multibranches
Jenkins -> New Element -> projet Pipeline Multibranches
exemple de nom:  project_demo_calculatrice_bagotd
Remplir ces champs:
- Display Name (libre)
- Description (libre)
- Branches Sources -> Git 
    - Project Repository: git@gitlab.cnes.fr:sandbox_qualite/calculatrice.git
    **Attention** S'assurer que l'adresse soir cohérente avec l'accès choisi (ici via SSH)
    - Credentials: Token du sous-group gitlab SandBox Qualite -> Ajouter
        - Folder Credentilas Provider: SANDOX_Qualite "bagotd_SandBox" project_demo_calculatrice_python
            - Domain Identifiants globaux (illimité)
            - Type SSH Username with private key
            - ID:  (ex *gitlab-jenkins-acces-key*, cf ci-dessus)
            - ne pas préciser de username
            - Private key Enter directly -> recopier le contenu du fichier ~/.ssh/gitlab-jenkins-acces-key (commence par "-----BEGIN OPENSSH PRIVATE KEY-----" et finit par "-----END OPENSSH PRIVATE KEY-----")
            - passphrase (ne pas se tromper parmi les 6 mots !)
)

A cette étape la lancement d'un build devrait montrer un accès de Jenkins à Gitlab 

## 3.2 Executer le code Python

Il s'agit ici de lancer le "main" et ensuite de lancer les tests unitaires.
Le fichier JenkinsFile (issu de la version fournie par défaut) doit etre configuré "au plus simple", avec peu d'étapes:
- Cleaning 
- Clone sources
- Init                -> sélectionner et afficher les versions des outils
- Build & Test        -> lancer *src/calcule.py* et les test unitaires sous *tests/*

Il suffit de mettre l'étape ("stage") permettant de lancer les commandes de lancement.

```python

       stage('Init') {
            steps {
                sh '''
                echo "Initialisation..."
                module avail python
                id -a 
                pwd
                ls -al
                python --version
                python3 --version

                module load python/3.8.4
                python3 --version
                '''
            }
        }

        // Etapes de Build et de tests unitaires
        stage('Build & Test') {
            steps {
                echo 'Building and Testing..'
                sh """
                echo "Lancement du Main..."
                python3 src/calcule.py

                echo "Lancement des tests unitaires..."
                python3 -m unittest tests/test_calculatrice.py
                """                    
            }
        }
```


<img src="jenkins_execution_simple_ok.png" alt="jenkins_execution_simple_ok.png" width="200"/>


# 4. SonarQube


### 4.1 Permettre à Jenkins l'accès à SonarQube

Source: https://confluence.cnes.fr/pages/viewpage.action?pageId=26171315

L'objectif de cette étape est de créer un Token de connexion à SonarQube. Ce Token sera lié à votre compte et il permettra à Jenkins (via le sonar-scanner) de lancer une analyse SonarQube.

1. Accéder à l'espace de votre projet Jenkins "projet_demao_calculatrice_<compte>"

2. Accéder à la page de gestion des "Identifiants"
<img src="credentials_jenkins.png" alt="credentials_jenkins" width="200"/>

3. Créer un "secret" de type "Secret text" en suivant la procédure [ici](https://confluence.cnes.fr/display/USINELOG/Jenkins+-+Gestion+des+identifiants+et+des+secrets#JenkinsGestiondesidentifiantsetdessecrets-Secrettext) : 
    - Son type : Secret text
    - Le texte à stocker (Secret)
    - Un identifiant (ou ID) : unique, clair, sans espace et avec un minimum de caractères spéciaux, par exemple : <compte>_SONAR_TOKEN, tel que identifié dans le **Jenkinsfile**:
    ```
        environment { ... SONAR_TOKEN=credentials('<compte>_SONAR_TOKEN')
    ```
    - Une description : à défaut recopier l'identifiant sinon expliciter l'utilité, la provenance du secret ou une instruction de renouvellement

    ### 4.2 Modifier le Jenkinsfile pour lancer sonarqube

    1. Reprendre l'exemple de demo en Java
    https://gitlab.cnes.fr/usinelogicielle/public/sensibilisations/project_demo-hello-world-getting-started/-/blob/master/Jenkinsfile

    y recopier les lignes relatives à sonarqube 

    2. Lancer un build