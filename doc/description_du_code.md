## Description du projet python


### But et localisation du code dans Gitlab

son  but est d'utiliser des fonctions arithmétiques pour faire des calcul, par exemple additionner 2 entiers.

Le code se trouve dans le Gitlab du CNES, dans le groupe sandbox_qualite: https://gitlab.cnes.fr/sandbox_qualite/.

La version de démo se trouve sous master.

La dernière version en développement est sous https://gitlab.cnes.fr/sandbox_qualite/calculatrice/-/tree/develop.

### Les principaux fichiers

#### Le code 

Le fichier *src/calcule.py* lance le programme principal.
Dans cet implémentation, il s'agit de calculer 1+1:
```py
    calculette = calculatrice.Calculatrice()
    resultat = calculette.additionner (1,1)
```` 

Le fichier *src/calculatrice.py* contient la classe conteant les fonctions arithmétiques, par exemple l'addition ci-dessus:
```py
class Calculatrice:
    def additionner (self, a, b):
        return a+b
``` 

#### Les test unitaires

Ils se trouvent sous tests/, dans le fichier *tests_calculatruce.py*.

Ces tests utilisent le framework unitests et sont compatibles avec pytest.

Il s'agit juste de vérifier que "1 + 1 = 2":

```py

self.assertEqual(2,self.cal.additionner(1,1),"l'addition des deux nombres est mauvaise")
```
